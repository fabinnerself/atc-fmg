<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    
    include_once 'database.php';
    include_once '02comprobantes.php';
    $database = new Database();
    $db = $database->getConnection();
    $items = new Comprobante($db);

    $items->id = isset($_GET['id']) ? $_GET['id'] : die();
    //echo "<br> id <br>".$items->id;
    
    
    $stmt = $items->getOneComprobante();
    $itemCount = $stmt->rowCount();

    //echo json_encode($itemCount);

    if($itemCount > 0){
        
        $ComprobanteArr = array();

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            extract($row);
            $e = array(
                "id" => $id,
                "remite_nom" => $remite_nom,
                "dest_nom" => $dest_nom,
                "tipo_cambio" => $tipo_cambio,
                "monto_mn" => $monto_mn, 
                "monto_me" => $monto_me,
                "fecha" => $fecha,
                "id_remitente" => $id_remitente,
                "id_destinatario" => $id_destinatario
            );

            array_push($ComprobanteArr, $e);
        }
        header('Content-type: application/json');
        echo json_encode($ComprobanteArr);
    }
    else{
        http_response_code(404);
        echo json_encode(
            array("message" => "No record found.")
        );
    }
?>