$(document).ready(function() {

  loadCompoListing();

  var addBtn = document.getElementById("addBtn");

addBtn.addEventListener("click", function() {
  
    window.location.href = "02addcomp.html";
});

 

$(document).on("click", ".editBtn", function() {
 // Obtener los datos de la fila correspondiente
  var row = $(this).closest("tr");
  var id = row.find("td:eq(0)").text();
  var fecha = row.find("td:eq(1)").text();
  var remitente = row.find("td:eq(2)").text();
  var destinatario = row.find("td:eq(3)").text();
  var tipoCambio = row.find("td:eq(4)").text();
  var montoBs = row.find("td:eq(5)").text();
  var montoUsd = row.find("td:eq(6)").text();
  var idRemitente = row.find("td:eq(7)").text();
  var idDestinatario = row.find("td:eq(8)").text();

 // Redireccionar a 02addcomp.html con los datos de la fila como parámetros de URL
 var url = "02addcomp.html?id=" + id +
   "&fecha=" + encodeURIComponent(fecha) +
   "&remitente=" + encodeURIComponent(idRemitente) +
   "&destinatario=" + encodeURIComponent(idDestinatario) +
   "&tipoCambio=" + encodeURIComponent(tipoCambio) +
   "&montoBs=" + encodeURIComponent(montoBs) +
   "&montoUsd=" + encodeURIComponent(montoUsd);

  window.location.href = url;
});


 
    
  // Load employee listing
  function loadCompoListing() {
    var url = '02findAllComp.php';

    $.ajax({
      type: 'GET',
      url: url,
      success: function(response) {
          
        var cdList = $('#cdList');
        cdList.empty();          

        $.each(response, function(index, cd) {
          var row = '<tr>' +
                      '<td>' + cd.id + '</td>' +
                      '<td>' + cd.fecha + '</td>' +
                      '<td>' + cd.remite_nom + '</td>' +
                      '<td>' + cd.dest_nom + '</td>' +
                      '<td>' + cd.tipo_cambio + '</td>' +
                      '<td>' + cd.monto_mn + '</td>' +
                      '<td>' + cd.monto_me + '</td>' +
                      '<td class=hidden-column >' + cd.id_remitente + '</td>' +
                      '<td class=hidden-column>' + cd.id_destinatario + '</td>' +
                      '<td>' +
                        '<button  name=editBtn class="btn btn-info editBtn" data-id="' + cd.id + '">Editar</button> ' +
                        '<button name=deleteBtn class="btn btn-danger deleteBtn" data-id="' + cd.id + '" data-idremitente="' + cd.id_remitente + '" data-iddestiantario="' + cd.id_destinatario + '" >Eliminar</button>' +
                      '</td>' +
                    '</tr>';
          cdList.append(row);
     
        });
      }
    });
   
  }


  });
  