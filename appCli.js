$(document).ready(function() {
    // Load CD listing on page load
    loadCDListing();
    var a=1;
  
    // Submit form to add employe
    $('#addForm').submit(function(event) {
      event.preventDefault();

      var form = $(this);
      if(document.getElementById("operacion").value == "ADD"){

        var url = 'create.php';
    
        $.ajax({
          type: 'POST',
          url: url,
          data: form.serialize(),
          success: function(response) {
            a=2;           
            //location.reload();
            //loadCDListing();            
          }
        });
      }

      if(document.getElementById("operacion").value == "MOD"){
        var url = 'update.php';
        $.ajax({
          type: 'POST',
          url: url,
          data: form.serialize(),
          success: function(response) {
            a=3;
            loadCDListing();            
          }
        });        
      }
      
      form[0].reset();
      document.getElementById('grabar').innerHTML='Grabar Empleado';
      document.getElementById("operacion").value = "ADD";

    });
  
    // carga- 1 employe
    $(document).on('click', '.editBtn', function() {
      
      document.getElementById("operacion").value = "MOD";
      document.getElementById("id").value = $(this).data('id');
      var url = 'single_read.php?id=' + $(this).data('id');
  
      $.ajax({
        type: 'POST',
        url: url,
        success: function(response) {
          
          document.getElementById("name").value=response.name;
          document.getElementById("email").value=response.email;
          document.getElementById("age").value=response.age;
          document.getElementById("designation").value=response.designation;
       
          document.getElementById('grabar').innerHTML='Actualizar Empleado';
        }
      });
      
    });   


    // Delete employe
    $(document).on('click', '.deleteBtn', function() {
      var cdId = $(this).data('id');
      var url = 'delete.php?id=' + cdId;
  
      $.ajax({
        type: 'DELETE',
        url: url,
        success: function(response) {
          loadCDListing();
          location.reload();
        }
      });
    });
    
  
    // Load employee listing
    function loadCDListing() {
      var url = '01readCli.php';
  
      $.ajax({
        type: 'GET',
        url: url,
        success: function(response) {
            
          var cdList = $('#cdList');
          cdList.empty();          
  
          $.each(response, function(index, cd) {
            var row = '<tr>' +
                        '<td>' + cd.id + '</td>' +
                        '<td>' + cd.nombre + '</td>' +
                        '<td>' + cd.apellido + '</td>' +
                        '<td>' + cd.celular + '</td>' +
                        '<td>' +
                          '<button name=editBtn class="btn btn-info editBtn" data-id="' + cd.id + '">Editar</button> ' +
                          '<button name=deleteBtn class="btn btn-danger deleteBtn" data-id="' + cd.id + '">Eliminar</button>' +
                        '</td>' +
                      '</tr>';
            cdList.append(row);
       
          });
        }
      });
     
    }
  });
  