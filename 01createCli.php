<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
    include_once 'database.php';
    include_once 'clientes.php';
    $database = new Database();
    $db = $database->getConnection();
    $item = new Cliente($db);
     
    $data = json_decode(file_get_contents("php://input"));
    //echo "data ".$title;
     
/*
    $item->nombre = $_POST['nombre'];
    $item->apellido = $_POST['apellido'];
    $item->celular = $_POST['celular'];
    */   
    $item->nombre = $data->nombre;
    $item->apellido = $data->apellido;
    $item->celular = $data->celular;
    
    
    
    if($item->createCliente()){
        echo 'Cliente created successfully.';
    } else{
        echo 'Cliente could not be created.';
    }
?>