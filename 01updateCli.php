<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
    
    include_once 'database.php';
    include_once 'clientes.php';
    
    $database = new Database();
    $db = $database->getConnection();
    
    $item = new Cliente($db);
   /* 
    $data = json_decode(file_get_contents("php://input"));
  

    $item->id = $data->id;  

    // employee values
    $item->nombre = $data->nombre;
    $item->apellido = $data->apellido;
    $item->celular = $data->celular;
*/

    $item->id = $_POST['id'];
    $item->nombre = $_POST['nombre'];
    $item->apellido = $_POST['apellido'];
    $item->celular = $_POST['celular'];
//  */  
    
    if($item->updateCliente()){
        echo json_encode("Datos de Cliente updated.");
    } else{
        echo json_encode("Data could not be updated");
    }
?>