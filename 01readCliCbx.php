<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    
    include_once 'database.php';
    include_once 'clientes.php';
    $database = new Database();
    $db = $database->getConnection();
    $items = new Cliente($db);
    $stmt = $items->getClientesCombo();
    $itemCount = $stmt->rowCount();

    //echo json_encode($itemCount);

    if($itemCount > 0){
        
        $ClienteArr = array();

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            extract($row);
            $e = array(
                "value" => $id,
                "label" => $nombres 
            );

            array_push($ClienteArr, $e);
        }
        header('Content-type: application/json');
        echo json_encode($ClienteArr);
    }
    else{
        http_response_code(404);
        echo json_encode(
            array("message" => "No record found.")
        );
    }
?>