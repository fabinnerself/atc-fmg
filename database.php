<?php 
    class Database {
        private $host = "127.0.0.1";
        private $database_name = "test_3499808";
        private $username = "usr_gambarte";
        private $password = "abc123";
        public $conn;

        public function getConnection(){
            $this->conn = null;
            try{
                $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->database_name, $this->username, $this->password);
                $this->conn->exec("set names utf8");
                //echo "connected";
            }catch(PDOException $exception){
                echo "La Base de Datos no pudo ser conectada: " . $exception->getMessage();
            }
            return $this->conn;
        }
    }  
?>