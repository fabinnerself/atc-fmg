-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 01, 2023 at 09:21 AM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.0.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test_3499808`
--
CREATE DATABASE IF NOT EXISTS `test_3499808` DEFAULT CHARACTER SET utf16 COLLATE utf16_spanish_ci;
USE `test_3499808`;

-- --------------------------------------------------------

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
CREATE TABLE IF NOT EXISTS `cliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(20) NOT NULL,
  `apellido` varchar(30) NOT NULL,
  `celular` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf16 COLLATE=utf16_spanish_ci;

--
-- RELATIONSHIPS FOR TABLE `cliente`:
--

--
-- Dumping data for table `cliente`
--

INSERT INTO `cliente` (`id`, `nombre`, `apellido`, `celular`) VALUES
(21, 'John', 'Doe', '1234567890'),
(22, 'Jane', 'Smith', '9876543210'),
(23, 'Michael', 'Johnson', '1243987856'),
(24, 'Emily', 'Davis', '5555555555'),
(25, 'Robert', 'Wilson', '1111111111'),
(26, 'Jessica', 'Brown', '9999999999'),
(27, 'David', 'Miller', '9812436512'),
(28, 'Olivia', 'Taylor', '7777777777'),
(29, 'Daniel', 'Anderson', '8888888888'),
(30, 'Sophia', 'Thomas', '4444444444'),
(33, 'unamed', 'unapellido', '30125478'),
(39, 'unamed', 'unapellido', '30125478'),
(40, 'llamado', 'llapellido', '12398745'),
(41, 'unamed', 'unapellido', '30125478'),
(42, 'unamed', 'unapellido', '30125478'),
(43, 'unamed', 'unapellido', '30125478'),
(44, 'unamed', 'unapellido', '30125478'),
(45, 'unamed', 'unapellido', '30125478'),
(46, 'unamed', 'unapellido', '30125478'),
(47, 'unamed', 'unapellido', '30125478');

-- --------------------------------------------------------

--
-- Table structure for table `comprobante`
--

DROP TABLE IF EXISTS `comprobante`;
CREATE TABLE IF NOT EXISTS `comprobante` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` timestamp NOT NULL DEFAULT current_timestamp(),
  `id_remitente` int(11) DEFAULT NULL,
  `id_destinatario` int(11) DEFAULT NULL,
  `tipo_cambio` decimal(10,4) DEFAULT NULL,
  `monto_mn` decimal(20,2) DEFAULT NULL,
  `monto_me` decimal(20,2) DEFAULT NULL,
  `removido_flag` int(11) DEFAULT 0,
  `fecha_creacion` timestamp NOT NULL DEFAULT current_timestamp(),
  `fecha_modificacion` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_remitente` (`id_remitente`),
  KEY `id_destinatario` (`id_destinatario`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf16 COLLATE=utf16_spanish_ci;

--
-- RELATIONSHIPS FOR TABLE `comprobante`:
--   `id_remitente`
--       `cliente` -> `id`
--   `id_destinatario`
--       `cliente` -> `id`
--

--
-- Dumping data for table `comprobante`
--

INSERT INTO `comprobante` (`id`, `fecha`, `id_remitente`, `id_destinatario`, `tipo_cambio`, `monto_mn`, `monto_me`, `removido_flag`, `fecha_creacion`, `fecha_modificacion`) VALUES
(1, '2023-06-11 04:00:00', 24, 27, 50.0000, 50.00, 1.00, 0, '2023-06-30 16:21:03', '2023-06-30 16:21:05'),
(21, '1970-01-01 05:00:00', 21, 22, 30.5000, 18.02, 19.01, 0, '2023-06-30 19:37:01', NULL),
(22, '1970-01-20 15:40:01', 21, 22, 30.5000, 18.02, 19.01, 0, '2023-06-30 19:37:57', NULL),
(23, '1970-01-20 15:40:01', 21, 22, 30.5000, 18.02, 19.01, 1, '2023-06-30 19:40:09', NULL),
(24, '0000-00-00 00:00:00', 21, 22, 30.5000, 18.02, 19.01, 1, '2023-06-30 20:06:49', NULL),
(29, '2023-06-16 04:00:00', 21, 26, 6.9600, 6.96, 1.00, 0, '2023-06-30 22:05:59', NULL);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comprobante`
--
ALTER TABLE `comprobante`
  ADD CONSTRAINT `comprobante_ibfk_1` FOREIGN KEY (`id_remitente`) REFERENCES `cliente` (`id`),
  ADD CONSTRAINT `comprobante_ibfk_2` FOREIGN KEY (`id_destinatario`) REFERENCES `cliente` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

CREATE USER 'usr_gambarte'@'localhost' IDENTIFIED BY 'abc123';
GRANT ALL PRIVILEGES ON *.* TO 'usr_gambarte'@'localhost';
FLUSH PRIVILEGES;

CREATE USER 'usr_gambarte'@'localhost' IDENTIFIED BY 'abc123';
GRANT SELECT, INSERT, UPDATE, DELETE ON test_3499808.* TO 'usr_gambarte'@'localhost';
FLUSH PRIVILEGES;