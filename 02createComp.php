<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
    include_once 'database.php';
    include_once '02comprobantes.php';
    $database = new Database();
    $db = $database->getConnection();
    $item = new Comprobante($db);
     
    $data = json_decode(file_get_contents("php://input"));
    //echo "<br/>data ".$data;
     

    $item->fecha = $_POST['fecha'];
    $item->id_remitente = $_POST['id_remitente'];
    $item->id_destinatario = $_POST['id_destinatario'];
    $item->tipo_cambio = $_POST['tipo_cambio'];
    $item->monto_mn = $_POST['monto_mn'];
    $item->monto_me = $_POST['monto_me'];
    /* 
    $item->fecha = $data->fecha;
    //echo "<br/>fecha ".$data->fecha;
    $item->id_remitente = $data->id_remitente;
    $item->id_destinatario = $data->id_destinatario;
    $item->tipo_cambio = $data->tipo_cambio;
    $item->monto_mn = $data->monto_mn;
    $item->monto_me = $data->monto_me;
    */
    if($item->createComprobante()){
        echo 'Comprobante created successfully.';
    } else{
        echo 'Comprobante could not be created.';
    }
?>