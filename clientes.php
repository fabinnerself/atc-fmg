<?php
    class Cliente{
        // Connection
        private $conn;
        // Table
        private $db_table = "cliente";
        // Columns
        public $id;
        public $nombre;
        public $apellido;
        public $celular;
        // Db connection
        public function __construct($db){
            $this->conn = $db;
        }
        // GET ALL
        public function getClientes(){
            $sqlQuery = "SELECT id, nombre, apellido, celular FROM " . $this->db_table . "";
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->execute();
            return $stmt;
        }

        // GET combo clientes 
        public function getClientesCombo(){
            $sqlQuery = "SELECT id, CONCAT(nombre, ' ', apellido) nombres  FROM " . $this->db_table . "";
            //echo "<br> query: $sqlQuery";
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->execute();
            return $stmt;
        }        
        // CREATE
        public function createCliente(){
            $sqlQuery = "INSERT INTO
                        ". $this->db_table ."
                    SET
                        nombre = :nombre, 
                        apellido = :apellido, 
                        celular = :celular ";
            //echo "$sqlQuery";
            $stmt = $this->conn->prepare($sqlQuery);
        
            // sanitize
            $this->nombre=htmlspecialchars(strip_tags($this->nombre));
            $this->apellido=htmlspecialchars(strip_tags($this->apellido));
            $this->celular=htmlspecialchars(strip_tags($this->celular));
        
            // bind data
            $stmt->bindParam(":nombre", $this->nombre);
            $stmt->bindParam(":apellido", $this->apellido);
            $stmt->bindParam(":celular", $this->celular);
                    
            if($stmt->execute()){
               return true;
            }
            return false;
        }
        // READ single
        public function getSingleCliente(){
            $sqlQuery = "SELECT
                        id, 
                        nombre, 
                        apellido, 
                        celular
                      FROM
                        ". $this->db_table ."
                    WHERE 
                       id = ?
                    LIMIT 0,1";
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->bindParam(1, $this->id);
            $stmt->execute();
            $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);
            
            $this->nombre = $dataRow['nombre'];
            $this->apellido = $dataRow['apellido'];
            $this->celular = $dataRow['celular'];
            
        }        
        // UPDATE
        public function updateCliente(){
            $sqlQuery = "UPDATE
                        ". $this->db_table ."
                    SET
                        nombre = :nombre, 
                        apellido = :apellido, 
                        celular = :celular
                    WHERE 
                        id = :id";
        
            $stmt = $this->conn->prepare($sqlQuery);
        
            $this->nombre=htmlspecialchars(strip_tags($this->nombre));
            $this->apellido=htmlspecialchars(strip_tags($this->apellido));
            $this->celular=htmlspecialchars(strip_tags($this->celular));
            $this->id=htmlspecialchars(strip_tags($this->id));
        
            // bind data
            $stmt->bindParam(":nombre", $this->nombre);
            $stmt->bindParam(":apellido", $this->apellido);
            $stmt->bindParam(":celular", $this->celular);
            $stmt->bindParam(":id", $this->id);
        
            if($stmt->execute()){
               return true;
            }
            return false;
        }
        // DELETE
        function deleteCliente(){
            $sqlQuery = "DELETE FROM " . $this->db_table . " WHERE id = ?";
            $stmt = $this->conn->prepare($sqlQuery);
        
            $this->id=htmlspecialchars(strip_tags($this->id));
        
            $stmt->bindParam(1, $this->id);
        
            if($stmt->execute()){
                return true;
            }
            return false;
        }
    }
?>