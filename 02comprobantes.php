<?php
    class Comprobante{
        // Connection
        private $conn;
        // Table
        private $db_table = "Comprobante";
        // Columns
        public $id;
        public $fecha;
        public $id_remitente;
        public $id_destinatario;
        public $tipo_cambio;
        public $monto_mn;
        public $monto_me;

        public $remite_nom;
        public $dest_nom;

        // Db connection
        public function __construct($db){
            $this->conn = $db;
        }
        // GET ALL
        public function getComprobantes(){
            $sqlQuery = "  SELECT c.id,  CONCAT(r.nombre, ' ', r.apellido) remite_nom,
            CONCAT(d.nombre, ' ', d.apellido) dest_nom,
            c.tipo_cambio, c.monto_mn, c.monto_me , DATE_FORMAT(c.fecha, '%e/%c/%Y %H:%i:%s') fecha,
            c.id_remitente,c.id_destinatario
            FROM comprobante c
            INNER JOIN cliente r ON c.id_remitente = r.id
            INNER JOIN cliente d ON c.id_destinatario = d.id where removido_flag=0 order by id;";

            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->execute();
            return $stmt;
        }

        public function getOneComprobante(){
            $sqlQuery = "   SELECT c.id,  CONCAT(r.nombre, ' ', r.apellido) remite_nom,
            CONCAT(d.nombre, ' ', d.apellido) dest_nom,
            c.tipo_cambio, c.monto_mn, c.monto_me , DATE_FORMAT(c.fecha, '%e/%c/%Y') fecha,
            c.id_remitente , c.id_destinatario
            FROM comprobante c
            INNER JOIN cliente r ON c.id_remitente = r.id
            INNER JOIN cliente d ON c.id_destinatario = d.id
            WHERE c.id=:id and  removido_flag=0 ;";

            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->bindParam(':id', $this->id);
            $stmt->execute();
            return $stmt;
        }

        // CREATE
        public function createComprobante(){
            $sqlQuery = "INSERT INTO
                        ". $this->db_table ."
                    SET
                        fecha = :fecha, 
                        id_remitente = :id_remitente, 
                        id_destinatario = :id_destinatario,
                        tipo_cambio = :tipo_cambio,
                        monto_mn = :monto_mn,
                        monto_me = :monto_me                        
                         ";
            //echo "$sqlQuery";
            $stmt = $this->conn->prepare($sqlQuery);
        
            // sanitize
            $this->fecha=htmlspecialchars(strip_tags($this->fecha));
            $this->id_remitente=htmlspecialchars(strip_tags($this->id_remitente));
            $this->id_destinatario=htmlspecialchars(strip_tags($this->id_destinatario));
            $this->tipo_cambio=htmlspecialchars(strip_tags($this->tipo_cambio));
            $this->monto_mn=htmlspecialchars(strip_tags($this->monto_mn));
            $this->monto_me=htmlspecialchars(strip_tags($this->monto_me));

            //echo "<br>  fecha orig ".$this->fecha;

            $fechaFormateada = date('Y-m-d H:i:s', strtotime($this->fecha));

            //echo "<br>  fecha format $fechaFormateada";
        
            // bind data
            $stmt->bindParam(":fecha", $fechaFormateada);
            $stmt->bindParam(":id_remitente", $this->id_remitente);
            $stmt->bindParam(":id_destinatario", $this->id_destinatario);
            $stmt->bindParam(":tipo_cambio", $this->tipo_cambio);
            $stmt->bindParam(":monto_mn", $this->monto_mn);
            $stmt->bindParam(":monto_me", $this->monto_me);
                    
            //echo "<br> query$sqlQuery";
            //echo "<br> val fecha ".$this->fecha;

            if($stmt->execute()){
               return true;
            }
            return false;
        }
      
       
                  
        // UPDATE
        public function updateComprobante(){
            $sqlQuery = "UPDATE
                        ". $this->db_table ."
                    SET fecha = :fecha, 
                    id_remitente = :id_remitente, 
                    id_destinatario = :id_destinatario,
                    tipo_cambio = :tipo_cambio,
                    monto_mn = :monto_mn,
                    monto_me = :monto_me 
                    WHERE id = :id";
        
            $stmt = $this->conn->prepare($sqlQuery);
        
            $this->fecha=htmlspecialchars(strip_tags($this->fecha));
            $this->id_remitente=htmlspecialchars(strip_tags($this->id_remitente));
            $this->id_destinatario=htmlspecialchars(strip_tags($this->id_destinatario));
            $this->tipo_cambio=htmlspecialchars(strip_tags($this->tipo_cambio));
            $this->monto_mn=htmlspecialchars(strip_tags($this->monto_mn));
            $this->monto_me=htmlspecialchars(strip_tags($this->monto_me));

            //echo "<br>fecha orig 2: ".$this->fecha;

            $fechaFormateada = date('Y-m-d H:i:s', strtotime($this->fecha));
        
            //echo "<br>fecha format: $fechaFormateada";
            // bind data
            $stmt->bindParam(":fecha", $fechaFormateada);
            $stmt->bindParam(":id_remitente", $this->id_remitente);
            $stmt->bindParam(":id_destinatario", $this->id_destinatario);
            $stmt->bindParam(":tipo_cambio", $this->tipo_cambio);
            $stmt->bindParam(":monto_mn", $this->monto_mn);
            $stmt->bindParam(":monto_me", $this->monto_me);
            $stmt->bindParam(":id", $this->id);
        
            if($stmt->execute()){
               return true;
            }
            return false;
        }
        // DELETE
        function deleteComprobante(){
            $sqlQuery = "UPDATE " . $this->db_table . " SET removido_flag=1 WHERE id = ?";
            //echo "<br> query: $sqlQuery";
            $stmt = $this->conn->prepare($sqlQuery);
        
            $this->id=htmlspecialchars(strip_tags($this->id));
        
            $stmt->bindParam(1, $this->id);
        
            if($stmt->execute()){
                return true;
            }
            return false;
        }
    }
?>