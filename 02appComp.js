$(document).ready(function() {
    // Load CD listing on page load
    loadComboremitente();
    loadCombodestin();
    var a=0; 


    var urlParams = new URLSearchParams(window.location.search);
    //alert(urlParams);
    if(urlParams!=""){

      document.getElementById("operacion").value = "MOD"
      var auxfecha=urlParams.get("fecha");

      var parts = auxfecha.split(" "); // Dividir la fecha por " " y "/"
      var parts = parts[0].split("/");
      var formattedDate = parts[2] + "-" + parts[1].padStart(2, "0") + "-" + parts[0].padStart(2, "0");
      
      var id = urlParams.get("id");
      var fecha = formattedDate;

      var tipoCambio = urlParams.get("tipoCambio");
      var montoBs = urlParams.get("montoBs");
      var montoUsd = urlParams.get("montoUsd");

      // Llenar los campos del formulario con los datos de la fila
      $("#id").val(id);
      $("#fecha").val(fecha);
      $("#tipo_cambio").val(tipoCambio);
      $("#monto_mn").val(montoBs);
      $("#monto_me").val(montoUsd);
      $("#id_remitente option[value='" + remitente + "']").prop("selected", true);
      $("#id_destinatario option[value='" + destinatario + "']").prop("selected", true);

      document.getElementById('grabar').innerHTML='Actualizar';

    }

    if(urlParams==""){

      var fechaActual = new Date();

      var dia = fechaActual.getDate();
      var mes = fechaActual.getMonth() + 1; // Los meses empiezan desde 0, por lo tanto se suma 1
      var anio = fechaActual.getFullYear();
      
      // Asegurarse de que el día y el mes tengan siempre dos dígitos
      dia = (dia < 10) ? "0" + dia : dia;
      mes = (mes < 10) ? "0" + mes : mes;
      
      var fechaFormateada = anio + "-" + mes + "-" + dia;

      var fecha = fechaFormateada;
      $("#fecha").val(fecha);
      document.getElementById("operacion").value = "ADD"
      document.getElementById('grabar').innerHTML='Grabar';
    }    

    var addCancelBtn = document.getElementById("cancelarBtn");

    addCancelBtn.addEventListener("click", function() {      
       window.history.back();
       //window.location.href ='02comprobantes.html';
    });    


 // Submit form to add employe
 $('#addForm').submit(function(event) {
  event.preventDefault();

  validarCamposTC();
  validarCampoRemDes();

  var form = $(this);
  if(document.getElementById("operacion").value == "ADD"){
  
    if (confirm("Esta seguro de registrar") == true) {

      var url = '02createComp.php';

      $.ajax({
        type: 'POST',
        url: url,
        data: form.serialize(),
        success: function(response) {
          a=2;        
            alert("Comprobante grabado");                    
        }
      });      
      
    } 
  }

  if(document.getElementById("operacion").value == "MOD"){

    if (confirm("Esta seguro de actualizar") == true) {

      var url = '02updateComp.php';

      $.ajax({
        type: 'POST',
        url: url,
        data: form.serialize(),
        success: function(response) {
          a=3;        
            alert("Comprobante actualizado");                    
        }
      });      
      
    } 
        
  }
    
  window.location.href ='02comprobantes.html';

});    


    function loadComboremitente() {

        $.ajax({
            url: '01readCliCbx.php', // URL de la API REST
            method: 'GET', // Método HTTP (GET, POST, etc.)
            dataType: 'json', // Tipo de datos esperados en la respuesta (JSON en este caso)
            success: function(response) {
            // Generar las opciones de la lista desplegable
            var id_remitente = $('#id_remitente');
            $.each(response, function(index, option) {
                id_remitente.append('<option value="' + option.value + '">' + option.label + '</option>');
            });
            },
            error: function() {
            alert('Error al obtener las opciones de la API.');
            }
        });
    }

    function loadCombodestin() {

        $.ajax({
            url: '01readCliCbx.php', // URL de la API REST
            method: 'GET', // Método HTTP (GET, POST, etc.)
            dataType: 'json', // Tipo de datos esperados en la respuesta (JSON en este caso)
            success: function(response) {
            // Generar las opciones de la lista desplegable
            var id_destinatario = $('#id_destinatario');
            $.each(response, function(index, option) {
                id_destinatario.append('<option value="' + option.value + '">' + option.label + '</option>');
            });
            },
            error: function() {
            alert('Error al obtener las opciones de la API.');
            }
        });
    }        

    function validarCamposTC() {
      // Obtener los valores de los campos
      var tipoCambio = parseFloat(document.getElementById('tipo_cambio').value);
      var monedaNacional = parseFloat(document.getElementById('monto_mn').value);
      var monedaExtranjera = parseFloat(document.getElementById('monto_me').value);

      // Verificar la regla de validación
      if (monedaNacional !== tipoCambio * monedaExtranjera) {
        alert('La moneda nacional debe ser igual a tipo de cambio por moneda extranjera.');
        return false; // Evitar el envío del formulario
      }

      return true; // Permitir el envío del formulario
    }

    function validarCampoRemDes(){
      var dest = parseFloat(document.getElementById('id_destinatario').value);
      var remit = parseFloat(document.getElementById('id_remitente').value);

      if (dest == remit) {
        alert('El destinatario no puede ser igual al remitente.');
        return false; // Evitar el envío del formulario

      }
      return true; 
    }
});
  